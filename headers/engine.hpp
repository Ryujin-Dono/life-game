#ifndef ENGINE_HPP
#define ENGINE_HPP


#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <vector>
#include <deque>
#include <fstream>
#include <stdlib.h>
#include <iostream>


using namespace sf;
using namespace std;

class Engine {
private:
    // Window
    Vector2f resolution;
    RenderWindow window;
    int tailleMax;

    // Event 
    Event ev;

    // Gamestate
    bool isPaused;
    bool grille;

    // Tab 
    bool tabBool[60][30];
    Vector2i tabBoolPosTrue[60*30];
    int currentNbrTab;

    

    

public:
    // mehode Engine
    //constructeur
    Engine();

    // The main loop will be in the run function
    void run();


    // methode simulation 
    void drawTab();
    void updateWindow();
    Vector3i willDie(int x, int y);
    int calcVoisin(int x, int y);


};


#endif 
