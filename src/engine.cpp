#include <engine.hpp>
#include <vector>


Engine::Engine(){
    this->resolution = Vector2f(1200, 600);
    this->window.create(VideoMode(resolution.x, resolution.y), "LifeGame", Style::Titlebar | Style::Close);
    this->isPaused = true;   
    this->grille = true;
    this->tailleMax = 30*60;

    for (size_t i = 0; i < size(tabBool); i++)    {
        for (size_t y = 0; y < size(tabBool[i]); y++){
            tabBool[i][y] = false;
        }
    }

    for (size_t i = 0; i < size(tabBoolPosTrue); i++){
        tabBoolPosTrue[i] = Vector2i(-1,-1);
    }
    this->currentNbrTab = 0;
    
}


void Engine::run(){
    if(window.isOpen())
        drawTab();
        int framrate = 0;

    while (window.isOpen()){

        //Event
        while(window.pollEvent(ev)){
            switch (ev.type){ 
                case Event::Closed:
                    this->window.close();
                    break;

                case Event::KeyPressed:
                    if(ev.key.code == Keyboard::Escape){
                        this->window.close();
                        break;
                    }

                    if(ev.key.code == Keyboard::P){
                        this->isPaused = !this->isPaused;
                        break;
                    }

                    if(ev.key.code == Keyboard::G){
                        this->grille = !this->grille;
                        drawTab();
                        break;
                    }

                    if(ev.key.code == Keyboard::N){
                        // next step 
                        updateWindow();
                        break;
                    }


                case Event::MouseButtonPressed:
                    if(ev.mouseButton.button == Mouse::Left){
                        //lorsque l'on clique on inverse le boolean a la position du clique 
                        tabBool[ev.mouseButton.x/20][ev.mouseButton.y/20] = !tabBool[ev.mouseButton.x/20][ev.mouseButton.y/20];
                    
                        // si le boolean a la position apres le clique est true alors on l'ajoute dans notre tableau de position 
                        if(tabBool[ev.mouseButton.x/20][ev.mouseButton.y/20] ){
                            tabBoolPosTrue[currentNbrTab++] = Vector2i(ev.mouseButton.x/20, ev.mouseButton.y/20);
                        }
                        // sinon on le supprime de celui ci 
                        else{
                            
                            for (size_t i = 0; i < this->currentNbrTab; i++){
                                if(tabBoolPosTrue[i].x == ev.mouseButton.x/20 && tabBoolPosTrue[i].y == ev.mouseButton.y/20){
                                    for (size_t y = i; y < this->currentNbrTab-1; y++){
                                        tabBoolPosTrue[y] = tabBoolPosTrue[y+1];
                                        
                                    }
                                    this->currentNbrTab--;
                                    
                                }
                            }  

                        }                                          
                        drawTab();
                    }
            }
        }
        
        if(framrate >=100000){
                updateWindow();
                framrate = 0;
            }
        framrate++;

    }
        
    
}