#include <engine.hpp> 

void Engine::drawTab(){
    window.clear();

    RectangleShape rect(Vector2f(20,20));
    for (size_t i = 0; i < size(tabBool) ; i++){
        for (size_t y = 0; y < size(tabBool[i]); y++){
            
            rect.setPosition(Vector2f((i*20),(y*20)));
            if(tabBool[i][y])
                rect.setFillColor(Color::White);
            else
                rect.setFillColor(Color::Black);


            window.draw(rect);
        }
        
    }

    if(this->grille){
        for (size_t i = 1; i < 60; i++)
        {
            RectangleShape line(Vector2f(1,600));
            line.setFillColor(Color::Red);
            line.setPosition(Vector2f(i*20,0));

            window.draw(line);
        }

        for (size_t i = 1; i < 30; i++)
        {
            RectangleShape line(Vector2f(1200,1));
            line.setFillColor(Color::Red);
            line.setPosition(Vector2f(0,i*20));

            window.draw(line);
        }
    }

    window.display();

}

void Engine::updateWindow(){
    if(!isPaused){

        Vector3i temp[30*60];
        int currentTempMax = 0;

        int nbr = 0;

        for (size_t i = 0; i < currentNbrTab; i++){
            
        
            for (size_t x = tabBoolPosTrue[i].x-1; x <= tabBoolPosTrue[i].x+1; x++){
                for (size_t y = tabBoolPosTrue[i].y-1; y <= tabBoolPosTrue[i].y+1; y++){
                    
                    if(find( begin(temp), end(temp), Vector3i(0,x,y) ) == end(temp) && find( begin(temp), end(temp), Vector3i(1,x,y) ) == end(temp) && find( begin(temp), end(temp), Vector3i(2,x,y) ) == end(temp)){
                        nbr = calcVoisin(x, y);
                        if(nbr < 2 || nbr > 3)
                            temp[currentTempMax++] = Vector3i(0, x, y);
                        else if(nbr == 3)
                            temp[currentTempMax++] = Vector3i(1, x, y);
                        else
                            temp[currentTempMax++] = Vector3i(2, x, y);
                    }

                }
                    
            }
                      
        }

        currentNbrTab = 0;

        for (size_t i = 0; i < currentTempMax; i++){
            if(temp[i].x == 1){
                tabBool[temp[i].y][temp[i].z] = true;
                tabBoolPosTrue[currentNbrTab++] = Vector2i(temp[i].y, temp[i].z);
            }
            else if(temp[i].x == 2 && tabBool[temp[i].y][temp[i].z]){
                tabBool[temp[i].y][temp[i].z] = true;
                tabBoolPosTrue[currentNbrTab++] = Vector2i(temp[i].y, temp[i].z);
            }
            else
                tabBool[temp[i].y][temp[i].z] = false;             
                
        }
        
        for (size_t i = 0; i < currentNbrTab; i++)
        {
            cout << tabBoolPosTrue[i].x << " " << tabBoolPosTrue[i].y << endl;
        }

        cout << " -------------------------------------------- " << endl;
        
    
        drawTab();

    }
}


int Engine::calcVoisin(int x, int y){
    int nbrVoisin = 0;

    if(tabBool[x-1][y-1] && x-1 >=0 && y-1 >=0)
        nbrVoisin ++;

    if(tabBool[x-1][y] && x-1 >=0 )
        nbrVoisin ++;

    if(tabBool[x-1][y+1] && x-1 >=0 && y+1 <=29)
        nbrVoisin ++;

    if(tabBool[x][y-1] && y-1 >=0)
        nbrVoisin ++;

    if(tabBool[x][y+1] && y+1 <=29)
        nbrVoisin ++;

    if(tabBool[x+1][y-1] && x+1 <=59 && y-1 >=0)
        nbrVoisin ++;

    if(tabBool[x+1][y] && x+1 <=59)
        nbrVoisin ++;

    if(tabBool[x+1][y+1] && x+1 <=59 && y+1 <=29)
        nbrVoisin ++;

    return nbrVoisin ;
}




